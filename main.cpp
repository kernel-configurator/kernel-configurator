/*
    kernel-configurator
    Copyright (C) 2014  aongeeno <aongeeno@openmailbox.org>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <dirent.h>
#include <fstream>
#include <iostream>

namespace fs = boost::filesystem;
namespace po = boost::program_options;
using std::cerr;
using std::cout;
using std::endl;
using std::string;

int main(int argc, char* argv[])
{
    string srcPath;
    string configScriptPath;
    string configPath;

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("src", po::value<std::string>(&srcPath)->default_value("/usr/src/linux"), "kernel source dir")
        ("config-script", po::value<std::string>(&configScriptPath)->default_value("scripts/config"), "path to config script")
        ("config-dir", po::value<std::string>(&configPath)->default_value("/etc/kernel/configs"), "config dir");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 0;
    }

    cout << srcPath << '\n'
        << configScriptPath << '\n'
        << configPath << '\n';

    std::map<string, string> mapKernelConfig;

    fs::path p(configPath);
    if(!fs::exists(p)) {
        cerr << configPath << " does not exist\n";
        return 1;
    }

    if (!fs::is_directory(p)) {
        cerr << configPath << " not a directory\n";
        return 1;
    }

    std::vector<fs::path> v; // store files' paths,
    copy(fs::directory_iterator(p), fs::directory_iterator(), back_inserter(v));
    sort(v.begin(), v.end()); // sort, since directory iteration is not ordered on some file systems

    for(const fs::path f : v) {
        if(!(fs::is_regular(f) || fs::is_symlink(f)))
            continue;
 
        cout << "   " << f << '\n';

        std::ifstream input(f.string());

        for(string line; getline( input, line );) {
            string trimmedLine(line);
            boost::algorithm::trim(trimmedLine);

            if(trimmedLine.empty() || trimmedLine[0] == '#')
                continue;

            size_t pos = trimmedLine.find_first_of('=');
            if(pos == string::npos) {
                cerr << "Invalid line: " << line << endl;
                return 1;
            }

            string option = trimmedLine.substr(0, pos);
            string value = trimmedLine.substr(pos + 1, trimmedLine.length() - 1);
            string baseCmd = srcPath + "/" + configScriptPath + " --file " + srcPath + "/.config";
            string cmd;

            if(mapKernelConfig.count(option) > 0 && mapKernelConfig.at(option) != value) {
                cerr << "Duplicate option with different values: " << option << endl;
                return 1;
            }

            mapKernelConfig[option] = value;

            if(value == "y") { // enabled
                cmd = baseCmd + " -e " + option;
            } else if(value == "n") { // disabled
                cmd = baseCmd + " -d " + option;
            } else if(value == "m") { // module
                cmd = baseCmd + " -m " + option;
            } else if(value[0] == '"') { // string
                cmd = baseCmd + " --set-str " + option + ' ' + value;
            } else { // value
                cmd = baseCmd + " --set-val " + option + ' ' + value;
            }

            cout << cmd << "\n";
            int exitCode = system(cmd.c_str());
            if(exitCode != 0) {
                cerr << "Execute failed with exit code " << exitCode << endl;
                return 1;
            }
        }
    }

    return 0;
}
